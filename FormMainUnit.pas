unit FormMainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    TimerReadCommand: TTimer;
    Memo1: TMemo;
    Label3: TLabel;
    TimerAnswer: TTimer;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    LblEditReadCommandTimerInterval: TLabeledEdit;
    BitBtn1: TBitBtn;
    LblEditTimerAnswer: TLabeledEdit;
    BitBtn2: TBitBtn;
    LblEditAnswerString: TLabeledEdit;
    procedure TimerReadCommandTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure TimerAnswerTimer(Sender: TObject);
  private
    CommandFileAge: integer;
  public
    CommandFileName, AnswerFileName: string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
    TimerReadCommand.Interval:=StrToInt( LblEditReadCommandTimerInterval.Text);
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
     TimerAnswer.Interval:=StrToInt( LblEditTimerAnswer.Text);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  FileName: string;
  IniFile: TStringList;
begin
    IniFile:=TStringList.Create;
    try
        FileName := ExtractFileName(Application.ExeName);
        FileName := ChangeFileExt(FileName, '.ini');
        //showmessage(IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName))+FileName);
        IniFile.LoadFromFile(IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName))+FileName);

        CommandFileName:=IniFile.Values['CommandFile'];
        AnswerFileName:=IniFile.Values['AnswerFile'];

        label1.Caption:=CommandFileName;
        Label2.Caption:=AnswerFileName;

        LblEditReadCommandTimerInterval.Text:=intToStr(TimerReadCommand.Interval);
        LblEditTimerAnswer.Text:=intToStr(TimerAnswer.Interval);

        TimerReadCommand.Enabled:=True;
        CommandFileAge:=0;
    finally
        Inifile.Free;
    end;
end;

procedure TForm1.TimerAnswerTimer(Sender: TObject);
var
  CommandString: string;
  AnswerFile: TextFile;
begin
    TimerAnswer.Enabled:=False;
    CommandString:=LblEditAnswerString.Text;

    AssignFile(AnswerFile, AnswerFileName) ;
    try
        Rewrite(AnswerFile) ;
        Writeln(AnswerFile, CommandString) ;
    finally
        CloseFile(AnswerFile);
    end;

    Memo1.Lines.Add(CommandString);
end;

procedure TForm1.TimerReadCommandTimer(Sender: TObject);
var
  CommandString: string;
  CommandFile: TextFile;
  iFileAge: integer;
begin
    AssignFile(CommandFile, CommandFileName) ;
    try
        Reset(CommandFile) ;
        ReadLn(CommandFile, CommandString) ;
        iFileAge:=FileAge(CommandFileName);
    finally
        CloseFile(CommandFile) ;
    end;

    if CommandFileAge <> iFileAge then
    begin
        Memo1.Lines.Add(CommandString+': '+intToStr(iFileage));
        TimerAnswer.Enabled:=true;
    end;

    CommandFileAge:=iFileAge;

    //SvarCode:=GetStringFromString(SvarStrongur,';',0,1);
    //result:=SvarCode;

end;

end.
