object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'FR-terminal emulator'
  ClientHeight = 358
  ClientWidth = 610
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    610
    358)
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 9
    Top = 3
    Width = 47
    Height = 13
    Caption = 'Command'
  end
  object Memo1: TMemo
    Left = 8
    Top = 21
    Width = 330
    Height = 329
    Anchors = [akLeft, akTop, akBottom]
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 344
    Top = 16
    Width = 258
    Height = 334
    Caption = 'GroupBox1'
    TabOrder = 1
    object Label1: TLabel
      Left = 9
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 9
      Top = 43
      Width = 31
      Height = 13
      Caption = 'Label2'
    end
    object LblEditReadCommandTimerInterval: TLabeledEdit
      Left = 9
      Top = 95
      Width = 57
      Height = 21
      EditLabel.Width = 119
      EditLabel.Height = 13
      EditLabel.Caption = 'Read Command Interval '
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 72
      Top = 93
      Width = 75
      Height = 25
      Caption = 'Change'
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object LblEditTimerAnswer: TLabeledEdit
      Left = 9
      Top = 158
      Width = 57
      Height = 21
      EditLabel.Width = 130
      EditLabel.Height = 13
      EditLabel.Caption = 'Answer Command Interval '
      TabOrder = 2
    end
    object BitBtn2: TBitBtn
      Left = 72
      Top = 156
      Width = 75
      Height = 25
      Caption = 'Change'
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 3
      OnClick = BitBtn2Click
    end
    object LblEditAnswerString: TLabeledEdit
      Left = 9
      Top = 224
      Width = 138
      Height = 21
      EditLabel.Width = 66
      EditLabel.Height = 13
      EditLabel.Caption = 'Answer string'
      TabOrder = 4
      Text = '0;1703;0;0'
    end
  end
  object TimerReadCommand: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TimerReadCommandTimer
    Left = 184
    Top = 168
  end
  object TimerAnswer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerAnswerTimer
    Left = 184
    Top = 224
  end
end
